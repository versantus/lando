<?php
 $databases['default']['default'] = array (
   'database' => 'database',
   'username' => 'databaseUser',
   'password' => 'databasePassword',
   'host' => 'database',
   'port' => '3306',
   'driver' => 'mysql',
   'prefix' => '',
   'collation' => 'utf8mb4_general_ci',
 );
 $databases['migrate']['default'] = array (
   'database' => 'migrate',
   'username' => 'databaseUser',
   'password' => 'databasePassword',
   'host' => 'migrate_database',
   'port' => '3306',
   'driver' => 'mysql',
   'prefix' => '',
   'collation' => 'utf8mb4_general_ci',
 );

$settings['hash_salt'] = file_get_contents('../salt.txt');
$settings['config_sync_directory'] = "../config_export";

$settings['trusted_host_patterns'] = [
  '.local.versantus.co.uk$',
  ];

$settings['file_private_path'] = '../private';

// Force HTTPS
$_SERVER['HTTPS'] = 'on';
$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_addresses'] = array("172.18.0.2","172.18.0.4","172.26.0.4");

/**
 * Enable local development services and disable caches
 */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/default/development.services.yml';
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$settings['extension_discovery_scan_tests'] = FALSE;

$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/**
 * Force email to be re-routed
 */
$config['reroute_email.settings']['enable'] = TRUE;

// Prevent sites/default folder permissions from being protected on local env.
$settings['skip_permissions_hardening'] = TRUE;

// Show all errors
$config['system.logging']['error_level'] = 'all';


ini_set('session.cookie_secure', 1);
ini_set('session.cookie_samesite', 'None');
