// Append this to the end of settings.php

// Automatically include docker settings if running from a docker env
  if (getenv('VERSANTUS_ENV') == 'local') {
      // Versantus docker dev environment
      include $app_root . '/' . $site_path . '/settings.docker.php';
  }

// Allow local overrides in settings.local.php.  Ensure this is the last thing we do in settings.php
 if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
   include $app_root . '/' . $site_path . '/settings.local.php';
 }
