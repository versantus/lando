--- # OWASP ZAP automation configuration file, for more details see https://www.zaproxy.com/docs/(TBA)
env:                                      # The environment, mandatory
  contexts:                               # List of 1 or more contexts, mandatory
    - name: local lando site                     # Name to be used to refer to this context in other jobs, mandatory
      urls:
      - INSERT_DRUSH_OPTIONS_URI             # The top level url, mandatory, everything under this will be included.  In lando replace this value with env var ${DRUSH_OPTIONS_URI}
      includePaths:  # TBA: An optional list of regexes to include
      excludePaths:  # TBA: An optional list of regexes to exclude
      authentication:                     # TBA: In time to cover all auth configs
  parameters:
    failOnError: true                  # If set exit on an error
    failOnWarning: true               # If set exit on a warning
    progressToStdout: true             # If set will write job progress to stdout

jobs:
  - type: passiveScan-config           # Passive scan configuration
    parameters:
      maxAlertsPerRule: 10             # Int: Maximum number of alerts to raise per rule
      scanOnlyInScope: true            # Bool: Only scan URLs in scope (recommended)
      maxBodySizeInBytesToScan:        # Int: Maximum body size to scan, default: 0 - will scan all messages
    rules:                             # A list of one or more passive scan rules and associated settings which override the defaults
      # The full rules list is at https://www.zaproxy.org/docs/alerts/
      # Rules that we always want to ignore in lando
      - id: 10038
        threshold: "Off"
      - id: 10109
        threshold: "Off"
      - id: 10202
        threshold: "Off"
      # Informational rules
      - id: 10015
        threshold: "Off"
      - id: 10019
        threshold: "Off"
      - id: 10024
        threshold: "Off"
      - id: 10025
        threshold: "Off"
      - id: 10027
        threshold: "Off"
      - id: 10031
        threshold: "Off"
      - id: 10057
        threshold: "Off"
      - id: 90011
        threshold: "Off"
      - id: 90033
        threshold: "Off"
      # Low rules
      - id: 2
        threshold: "Off"
      - id: 10010
        threshold: "Off"
      - id: 10011
        threshold: "Off"
      - id: 10017
        threshold: "Off"
      - id: 10021
        threshold: "Off"
      - id: 10023
        threshold: "Off"
      - id: 10035
        threshold: "Off"
      - id: 10036
        threshold: "Off"
      - id: 10037
        threshold: "Off"
      - id: 10044
        threshold: "Off"
      - id: 10054
        threshold: "Off"
      - id: 10056
        threshold: "Off"
      - id: 10061
        threshold: "Off"
      - id: 10096
        threshold: "Off"
      - id: 10109
        threshold: "Off"
      - id: 10202
        threshold: "Off"

  - type: spider                       # The traditional spider - fast but doesnt handle modern apps so well
    parameters:
      context:                         # String: Name of the context to spider, default: first context
      url:                             # String: Url to start spidering from, default: first context URL
      failIfFoundUrlsLessThan:         # Int: Fail if spider finds less than the specified number of URLs, default: 0
      warnIfFoundUrlsLessThan:         # Int: Warn if spider finds less than the specified number of URLs, default: 0
      maxDuration:                     # Int: The max time in minutes the spider will be allowed to run for, default: 0 unlimited
      maxDepth:                        # Int: The maximum tree depth to explore, default 5
      maxChildren:                     # Int: The maximum number of children to add to each node in the tree
      acceptCookies:                   # Bool: Whether the spider will accept cookies, default: true
      handleODataParametersVisited:    # Bool: Whether the spider will handle OData responses, default: false
      handleParameters:                # Enum [ignore_completely, ignore_value, use_all]: How query string parameters are used when checking if a URI has already been visited, default: use_all
      maxParseSizeBytes:               # Int: The max size of a response that will be parsed, default: 2621440 - 2.5 Mb
      parseComments:                   # Bool: Whether the spider will parse HTML comments in order to find URLs, default: true
      parseGit:                        # Bool: Whether the spider will parse Git metadata in order to find URLs, default: false
      parseRobotsTxt:                  # Bool: Whether the spider will parse 'robots.txt' files in order to find URLs, default: true
      parseSitemapXml:                 # Bool: Whether the spider will parse 'sitemap.xml' files in order to find URLs, default: true
      parseSVNEntries:                 # Bool: Whether the spider will parse SVN metadata in order to find URLs, default: false
      postForm:                        # Bool: Whether the spider will submit POST forms, default: true
      processForm:                     # Bool: Whether the spider will process forms, default: true
      requestWaitTime:                 # Int: The time between the requests sent to a server in milliseconds, default: 200
      sendRefererHeader:               # Bool: Whether the spider will send the referer header, default: true
      threadCount:                     # Int: The number of spider threads, default: 2
      userAgent:                       # String: The user agent to use in requests, default: '' - use the default ZAP one

  - type: passiveScan-wait             # Passive scan wait for the passive scanner to finish
    parameters:
      maxDuration: 5                   # Int: The max time to wait for the passive scanner, default: 0 unlimited

  - type: outputSummary
    parameters:
      format: Long
      summaryFile: /app/owasp/summary.json

  - type: report
    parameters:
      reportDir: /app/owasp
      reportFile: owasp-report.html
      reportTitle: ZAP OWASP passive scan report
      reportDescription: 'Versantus note: You only need to address High and Medium issues.'
      template: traditional-html

#  - type: report
#    parameters:
#      reportDescription: ''
#      reportDir: /app/owasp
#      reportFile: owasp-report-high-level.html
#      reportTitle: ZAP OWASP passive scan high level report
#      template: high-level-report
#
#  - type: report
#    parameters:
#      reportDescription: ''
#      reportDir: /app/owasp
#      reportFile: owasp-report-modern.html
#      reportTitle: ZAP OWASP passive scan report
#      template: modern
#      theme: corporate


#  - type: activeScan                   # The active scanner - this actively attacks the target so should only be used with permission
#    parameters:
#      context:                         # String: Name of the context to attack, default: first context
#      policy:                          # String: Name of the scan policy to be used, default: Default Policy
#      maxRuleDurationInMins:           # Int: The max time in minutes any individual rule will be allowed to run for, default: 0 unlimited
#      maxScanDurationInMins:           # Int: The max time in minutes the active scanner will be allowed to run for, default: 0 unlimited
#      addQueryParam:                   # Bool: If set will add an extra query parameter to requests that do not have one, default: false
#      defaultPolicy:                   # String: The name of the default scan policy to use, default: Default Policy
#      delayInMs:                       # Int: The delay in milliseconds between each request, use to reduce the strain on the target, default 0
#      handleAntiCSRFTokens:            # Bool: If set then automatically handle anti CSRF tokens, default: false
#      injectPluginIdInHeader:          # Bool: If set then the relevant rule Id will be injected into the X-ZAP-Scan-ID header of each request, default: false
#      scanHeadersAllRequests:          # Bool: If set then the headers of requests that do not include any parameters will be scanned, default: false
#      threadPerHost:                   # Int: The max number of threads per host, default: 2
#    policyDefinition:                  # The policy definition - only used if the 'policy' is not set
#      defaultStrength: Medium    # String: The default Attack Strength for all rules, one of Low, Medium, High, Insane (not recommended), default: Medium
#      defaultThreshold: Medium  # String: The default Alert Threshold for all rules, one of Off, Low, Medium, High, default: Medium
#      rules:                           # A list of one or more active scan rules and associated settings which override the defaults
#      - id: 40012
