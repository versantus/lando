# Versantus Lando configuration files

## Using lando for the first time.
- The wiki contains instructions for [installing Lando](https://versantus.atlassian.net/wiki/spaces/VW/pages/2774073345/Lando#Installing-Lando-on-Linux-(Ubuntu))
  - Create a global config file at `~/.lando/config.yml`
- Create an alias in your ~/.bash_aliases file as follows:
  - `alias vits_setup="~/.lando/vits_setup.sh"`
  - Then restart your terminal for the alias to take effect
- Follow the [first time using Lando](https://versantus.atlassian.net/wiki/spaces/VW/pages/2774073345/Lando#First-time-using-Lando) instructions on the wiki for populating it
- Then move on to the instructions below for setting up a site.
- Once the site is set up you'll need to [tell your browser to accept the SSL cert](https://versantus.atlassian.net/wiki/spaces/VW/pages/2774073345/Lando#Resolving-browser-privacy-warning)
 

## Setting up Lando to run a site.

- If the site is Drupal 7 or wordpress, first get your `settings.php` or `wp-config.php` file in place.
  - For Drupal 9 sites, settings has already taken care of for you. (You're welcome.)

### If the repo already contains a .lando.yml file
- Check if the folder contains a `.lando.dist.yml` file.
  - If not, run the `vits_setup` script to create a `.lando.dist.yml` file.
  - ```shell
      vits_setup
      ```

- Start the container.
```shell
lando start
```

- Import a copy of the live db
```shell
lando vits_copy_live
```

- Done

### If the repo doesn't contain a .lando.yml

- Create a `.lando.yml` file
  - Download the template
  - ```shell
    curl -o .lando.yml https://bitbucket.org/versantus/lando/raw/master/landofiles/.lando.yml
    ```
  - Edit `.lando.yml` and replace the word 'example' with the settings for your site.
  - Tip: If the repo already contains a `docker-composer.yml` then you can copy the env variables from there. 

- Run the vits_setup script:
  - ```shell
    vits_setup
    ```
  - If you do not have this script then get it by running:
    - ```shell
      curl -o ~/.lando/vits_setup.sh https://bitbucket.org/versantus/lando/raw/master/scripts/vits_setup.sh
      chmod +x ~/.lando/vits_setup.sh
      ~/.lando/vits_setup.sh
      ```
    - I recommend you add an alias to your ~/.bash_aliases file as follows:
      ```shell
      alias vits_setup="~/.lando/vits_setup.sh"
      ```

- Start the container. 
```shell
lando start
```

- Import a copy of the live db
```shell
lando vits_copy_live
```
or 
```shell
lando db-import [sql dump file]
```
The sql dump file needs to be within the project for lando to import it. 

- Done!


***This is the way.***

