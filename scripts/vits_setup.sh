#!/usr/bin/env bash

# Versantus lando setup script.
# Provisions lando yml and config files and ensures your system is up to date with
# the latest lando config.

# Copy each file in the lando repo into the ~/.lando/vits folder.

WHITE="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

echo-red () { echo -e "${RED}$1${WHITE}"; }
echo-green () { echo -e "${GREEN}$1${WHITE}"; }
echo-yellow () { echo -e "${YELLOW}$1${WHITE}"; }

REPO="https://bitbucket.org/versantus/lando"
LANDO_DIR="${HOME}/.lando"
CONFIG_ERROR=0

GLOBAL_YML="${LANDO_DIR}/config.yml"
PROJECT_YML=".lando.yml"
AUTH_JSON="auth.json"

if [ -f vits_pre_setup.sh ]; then
    echo-green "Running vits_pre_setup."
    chmod +x ${PWD}/vits_pre_setup.sh
    ${PWD}/vits_pre_setup.sh
    EXIT_CODE=$?
    if [ "${EXIT_CODE}" == "1" ]; then
	exit 1;
    fi
elif [ -f vits_pre_setup ]; then
    echo-green "Running vits_pre_setup."
    chmod +x ${PWD}/vits_pre_setup
    ${PWD}/vits_pre_setup
    EXIT_CODE=$?
    if [ "${EXIT_CODE}" == "1" ]; then
	exit 1;
    fi
fi

echo-green "Running vits setup."

# Check global config exists
if [ ! -f ${GLOBAL_YML} ]; then
  echo-red "No config.yml file found in the ${LANDO_DIR} directory.  Please create this file before running this script."
  echo-red "  ■  Follow the [first time using Lando](https://versantus.atlassian.net/wiki/spaces/VW/pages/2774073345/Lando#First-time-using-Lando) instructions on the wiki for populating it."
  exit 1
fi

# Validate config.yml
echo -e " ■  Checking that ${YELLOW}${GLOBAL_YML}${WHITE} contains domain proxy"
if ! grep -qxF 'proxyCommand:' ${GLOBAL_YML}; then
  echo-red " ■  Missing proxyCommand from ${GLOBAL_YML}"
  CONFIG_ERROR=1
fi

echo -e " ■  Checking that ${YELLOW}${GLOBAL_YML}${WHITE} contains BACKUP env vars"
if ! grep -qF 'VERSANTUS_ENV: local' ${GLOBAL_YML}; then
  echo-red " ■  Missing VERSANTUS_ENV: local from ${GLOBAL_YML}"
    CONFIG_ERROR=1
  fi
if ! grep -qF 'VERSANTUS_DEV_EMAIL:' ${GLOBAL_YML}; then
  echo-red " ■  Missing VERSANTUS_DEV_EMAIL from ${GLOBAL_YML}"
  CONFIG_ERROR=1
fi
if ! grep -qF 'BACKUP_SERVER:' ${GLOBAL_YML}; then
  echo-red " ■  Missing BACKUP_SERVER: from ${GLOBAL_YML}"
  CONFIG_ERROR=1
fi
if ! grep -qF 'BACKUP_SERVER_PORT:' ${GLOBAL_YML}; then
  echo-red " ■  Missing BACKUP_SERVER_PORT: from ${GLOBAL_YML}"
  CONFIG_ERROR=1
fi

if ! grep -qF 'BACKUP_SERVER_USER:' ${GLOBAL_YML}; then
  echo-red " ■  Missing BACKUP_SERVER_USER: from ${GLOBAL_YML}"
  CONFIG_ERROR=1
fi
if ! grep -qF 'BACKUP_SERVER_DIR:' ${GLOBAL_YML}; then
  echo-red " ■  Missing BACKUP_SERVER_DIR: from ${GLOBAL_YML}"
  CONFIG_ERROR=1
fi

if [ "${CONFIG_ERROR}" == "1" ]; then
  exit 1
fi

# Check auth.json exists
echo -e " ■  Checking that ${YELLOW}${LANDO_DIR}/${AUTH_JSON}${WHITE} exists"
if [ ! -f ${LANDO_DIR}/${AUTH_JSON} ]; then
  echo-red
  echo-red " No auth.json file found in directory ${LANDO_DIR}.  Please create an auth.json as follows:"
  echo -e " ■  Create a new file in ${LANDO_DIR}/${AUTH_JSON}"
  echo -e " ■  Paste the content found in LastPass under auth.json"
  echo -e " ■  Run ${YELLOW}vits_setup${WHITE}"
  exit 1
fi

# Check .lando.yml exists
if [ ! -f ${PROJECT_YML} ]; then
  echo-red " No .lando.yml file found in the current directory.  Please run this script from the directory that your project is located."
  echo " If this is a new project then please create a .lando.yml file before running this script."
  echo " ■  Download the template"
  echo "    curl -o .lando.yml https://bitbucket.org/versantus/lando/raw/master/landofiles/.lando.yml"
  echo "  ■  Edit .lando.yml and replace the word 'example' with the settings for your site."
  echo "  ■  If the repo already contains a docker-composer.yml then you can copy the env variables from there."
  exit 1
fi

# Validate .lando.yml
echo -e " ■  Checking that ${YELLOW}${PROJECT_YML}${WHITE} contains required env vars "

# Work out what recipe the current site is using.
RECIPE=$(grep \^recipe\: .lando.yml | tail -n 1 | sed  -e 's/recipe://' | sed -e 's/ *//g')
if [ "${RECIPE}" == "" ]; then
  echo-red " No recipe has been defined in the .lando.yml file."
  echo "  ■  Download the template"
  echo "    curl -o .lando.yml https://bitbucket.org/versantus/lando/raw/master/landofiles/.lando.yml"
  echo "  ■  Edit .lando.yml and replace the word 'example' with the settings for your site."
  echo "  ■  If the repo already contains a docker-composer.yml then you can copy the env variables from there."
  exit 1;
fi
echo -e " ■  This site is using recipe: ${GREEN}${RECIPE}${WHITE}"

# Check that the site has been given a name
NAME=$(grep \^name\: .lando.yml | tail -n 1 | sed  -e 's/name://' | sed -e 's/ *//g')
echo -e " ■  This site is using set up with name: ${GREEN}${NAME}${WHITE}"
if [ "${NAME}" == "" ]; then
  echo-red " ■  No name has been defined in the .lando.yml file."
  echo "  ■  Download the template"
  echo "    curl -o .lando.yml https://bitbucket.org/versantus/lando/raw/master/landofiles/.lando.yml"
  echo "  ■  Edit .lando.yml and replace the word 'example' with the settings for your site."
  echo "  ■  If the repo already contains a docker-composer.yml then you can copy the env variables from there."
  exit 1;
fi
if [ "${NAME}" == "example" ]; then
  echo-red " ■  Name is set to the default 'example' from the template."
  echo "  ■  Please configure the .lando.yml file for your project before proceeding."
  exit 1;
fi


if [[ "${RECIPE}" =~ "drupal" || "${RECIPE}" =~ "pantheon" ]]; then
  if ! grep -qF 'DRUSH_OPTIONS_URI:' ${PROJECT_YML}; then
    echo-yellow " ■  Missing DRUSH_OPTIONS_URI: from ${PROJECT_YML}"
  fi
  if ! grep -qF 'LIVE_SITE_URL:' ${PROJECT_YML}; then
    echo-yellow " ■  Missing LIVE_SITE_URL: from ${PROJECT_YML} - you won't be able to run lando vits_copy_live"
  fi

  WEBROOT=$(grep "webroot\:" .lando.yml | tail -n 1 | sed  -e 's/webroot://' | sed -e 's/ *//g' | sed -e 's/\"*//g')
  if [ "${WEBROOT}" == "" ]; then
    echo-red " ■  No webroot has been defined in your .lando.yml."
    echo "  ■  If your site will be hosted on Acquia and/or uses 'docroot' as the webroot add:"
    echo-yellow "config:"
    echo-yellow "  webroot: docroot "
    echo "  ■  If your site is legacy Drupal 9 and hosted on linode then add:"
    echo-yellow "config:"
    echo-yellow "  webroot: drupal/web"
    echo "  ■  If your site is Drupal 7 add:"
    echo-yellow "config:"
    echo-yellow "  webroot: public_html"
    echo-red " ■  !!! vits_setup has not completed !!! You need to add the webroot to proceed."
    exit 1;
  else
    echo -e " ■  This site has its webroot at: ${GREEN}${WEBROOT}${WHITE}";
  fi

  if ! grep -qF 'THEME_DIR:' ${PROJECT_YML}; then
    echo-yellow " ■  Missing THEME_DIR: from ${PROJECT_YML} - you won't be able to run front-end tooling scripts like lando compile-css"
    echo-yellow "   ■  To fix this, add services: -> node: -> overrides: -> environment: -> THEME_DIR: /app/${WEBROOT}/themes/custom/[your_theme_folder] to .lando.yml"
  fi
fi

# Clone / pull the repo.
if [ ! -d ${LANDO_DIR}/vits/.git ]; then
  echo -e " ■  Cloning versantus lando config repo "
  echo "    git clone ${REPO} ${LANDO_DIR}/vits"
  git clone ${REPO} ${LANDO_DIR}/vits
else
  echo -e " ■  Checking out versantus lando config repo "
  #echo -e "${PURPLE}git -C ${LANDO_DIR}/vits switch -f -B master"
  git -C ${LANDO_DIR}/vits fetch --all 1>/dev/null 2>/dev/null
  git -C ${LANDO_DIR}/vits reset --hard origin/master 1>/dev/null 2>/dev/null
  git -C ${LANDO_DIR}/vits checkout -f -B master 1>/dev/null 2>/dev/null
fi

# Set ownership/permissions
find ${LANDO_DIR}/vits/scripts -type f -exec chmod 777 {} \;
find ${LANDO_DIR}/vits/scripts -type d -exec chmod 775 {} \;


### Now we've got all our assets up to date, lets configure this project.

echo -e " ■  Copying ${RECIPE} ${YELLOW}.lando.dist.yml${WHITE} into this project}"
LANDO_DIST_FILE="${LANDO_DIR}/vits/landofiles/${RECIPE}/.lando.dist.yml"
LOCAL_LANDO_FILE=".lando.dist.yml"
# Replace instances of ${WEBROOT} in the dist file with the actual WEBROOT value.
cat ${LANDO_DIST_FILE} | sed  -e "s#\${WEBROOT}#${WEBROOT}#g" > ${LOCAL_LANDO_FILE}

echo -e " ■  Copying ${YELLOW}.lando.php.ini${WHITE} into this project"
LANDO_DIST_FILE="${LANDO_DIR}/vits/config/.lando.php.ini"
LOCAL_LANDO_FILE=".lando.php.ini"
cp ${LANDO_DIST_FILE} ${LOCAL_LANDO_FILE}


# Update .gitignore if it isn't already lando-aware
if [ -f .gitignore ]; then
  echo -e " ■  Updating ${YELLOW}.gitignore"
  # Add newline to the end of file if there isn't one already
  [[ $(tail -c1 .gitignore) && -f .gitignore ]]&&echo ''>>.gitignore
  grep -qxF '.lando.local.yml' .gitignore || echo '.lando.local.yml' >> .gitignore
  grep -qxF '.lando.dist.yml' .gitignore || echo '.lando.dist.yml' >> .gitignore
  grep -qxF '.lando.php.ini' .gitignore || echo '.lando.php.ini' >> .gitignore
fi

echo-green "vits setup complete"

echo -e "${YELLOW}Now run ${GREEN}lando rebuild -y${YELLOW} to start up the container.${WHITE}"

if [ -f vits_post_setup.sh ]; then
    echo-green "Running vits_post_setup."
    chmod +x ${PWD}/vits_post_setup.sh
    ${PWD}/vits_post_setup.sh
    EXIT_CODE=$?
    if [ "${EXIT_CODE}" == "1" ]; then
	exit 1;
    fi
elif [ -f vits_post_setup ]; then
    echo-green "Running vits_post_setup."
    chmod +x ${PWD}/vits_post_setup
    ${PWD}/vits_post_setup
    EXIT_CODE=$?
    if [ "${EXIT_CODE}" == "1" ]; then
	exit 1;
    fi
fi
