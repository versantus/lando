#!/usr/bin/env bash

NORMAL="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

echo
echo -e "${YELLOW}Tailing logs...${NORMAL}"
echo
tail -f /app/logs/drupal/drupal.log | tr -s '|' \\t
