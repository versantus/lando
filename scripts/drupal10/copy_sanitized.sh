#!/usr/bin/env bash

WHITE="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

echo-red () { echo -e "${RED}$1${WHITE}"; }
echo-green () { echo -e "${GREEN}$1${WHITE}"; }
echo-yellow () { echo -e "${YELLOW}$1${WHITE}"; }

if [ -d "/app/vendor" ]; then
  DRUSH="/app/vendor/drush/drush/drush --root=/app/docroot -y";
fi

if [ -d "/app/drupal/vendor" ]; then
   DRUSH="/app/drupal/vendor/drush/drush/drush --root=/app/drupal/web -y";
fi

if [ -z "$DRUSH" ]; then
  echo "Warning: DRUSH variable is not set and Drush executable not found.";
fi

if [ ! -d /app/sql ]; then mkdir /app/sql; fi

if [ "${ACQUIA_APPLICATION}" != "" ]; then PRIMARY_SITE_DOMAIN="${ACQUIA_APPLICATION}"; fi

echo-yellow " Copying database from backup server ... "
rsync -avlhP -e "ssh -p ${BACKUP_SERVER_PORT}" ${BACKUP_SERVER_USER}@${BACKUP_SERVER}:${BACKUP_SERVER_DIR}/sanitized/${PRIMARY_SITE_DOMAIN}.sql.gz /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz
if [ $? -eq 0 ]; then
  : # success
else
  echo-red " Sorry, something went wrong.  Couldn't find copied db backup - expected /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz.  Command to download it was: ${YELLOW}rsync -avlhP -e \"ssh -p ${BACKUP_SERVER_PORT}\" ${BACKUP_SERVER_USER}@${BACKUP_SERVER}:${BACKUP_SERVER_DIR}/sanitized/${PRIMARY_SITE_DOMAIN}.sql.gz /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz ";
  echo-red " Please check the PRIMARY_SITE_DOMAIN variable in your .lando.yml file.";
  echo-red " It could be this site does not have a sanitized db.  If this is the case try vits_copy_unsanitized instead. ";
  exit;
fi

DB_CHECK=`zcat /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz | tail -n 2 | grep "SET FOREIGN_KEY_CHECKS = 1" | wc -l`
if [ "${DB_CHECK}" == "0" ]; then
  echo-red " Sorry, something went wrong.  Couldn't find 'SET FOREIGN_KEY_CHECKS = 1' in sql file.  Command was: ${YELLOW}zcat /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz | tail -n 2 | grep "UNLOCK TABLES" | wc -l ";
  exit;
fi

echo-yellow "   Database copied to /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz "

echo-yellow " Extracting database from gz ... "
gunzip -kf /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz

echo-yellow " Dropping existing tables ... "
${DRUSH} sql-drop

echo-yellow " Importing database dump ... "
pv /app/sql/${PRIMARY_SITE_DOMAIN}.sql | ${DRUSH} sqlc
rm /app/sql/${PRIMARY_SITE_DOMAIN}.sql

# Fix user 0 entry in users table, if there ought to be one.  https://versantus.atlassian.net/browse/DEVOPS-89
USER0_SHOULD_EXIST=`${DRUSH} sql-query "SELECT COUNT(uid) FROM users_field_data WHERE uid=0;" | sed -e 's/[[:space:]]*//g'`
USER0_DOES_EXIST=`${DRUSH} sql-query "SELECT COUNT(uid) FROM users WHERE uid=0;" | sed -e 's/[[:space:]]*//g'`

if [ "${USER0_SHOULD_EXIST}" = "1" ]
then
    if [ "${USER0_DOES_EXIST}" = "0" ]
    then
        echo "Fixing user 0"
        RESULT=`${DRUSH} sql-query "UPDATE users SET uid=0 WHERE uid=(SELECT MAX(uid) FROM users) LIMIT 1"`
        USER0_NOW_EXISTS=`${DRUSH} sql-query "SELECT COUNT(uid) FROM users WHERE uid=0;" | sed -e 's/[[:space:]]*//g'`
        if [ "${USER0_NOW_EXISTS}" = "1" ]
        then
	          echo "Fixed users table entry for user 0"
        else
	          echo "Failed to fix user 0"
        fi
    fi
    # Fix mail/password for user 0
    RESULT=`${DRUSH} sql-query "UPDATE users_field_data SET mail=NULL, pass=NULL, name='',init=NULL WHERE uid=0;"`
fi


echo-yellow " Updating database ... "
${DRUSH} updatedb

echo-yellow " Enabling stage file proxy ... "
${DRUSH} en stage_file_proxy
${DRUSH} cset stage_file_proxy.settings origin ${LIVE_SITE_URL}

echo-yellow " Clearing cache ... "
${DRUSH} cr

if [ -f /app/.lando.post_copy_live.sh ]; then
    echo-yellow " Running /app/.lando.post_copy_live.sh ..."
    chmod +x /app/.lando.post_copy_live.sh
    /app/.lando.post_copy_live.sh
fi

echo-yellow " Done! Here's an admin login link: ${GREEN} ` ${DRUSH} uli`"
#echo-yellow " If you are not the develop branch or have locally modified config you may want to run ${GREEN}lando drush cim${YELLOW} now. "
 

