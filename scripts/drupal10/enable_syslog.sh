#!/usr/bin/env bash

NORMAL="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

#DRUSH="/app/vendor/drush/drush/drush --root=/app/docroot -y "

# Script to enable the syslog module and start the rsyslog service as part of lando build

echo
echo -e "${YELLOW}Enabling syslog module ...${NORMAL}"
echo

ALREADY_SETUP=`grep -F 'local7.* /app/logs/drupal/drupal.log' /etc/rsyslog.conf`

echo
echo -e "${YELLOW}Checking if syslog is already installed ... ${NORMAL}"
echo


if [ "${ALREADY_SETUP}" == "" ]; then
        echo
        echo -e "${YELLOW}Installing syslog module ...${NORMAL}"
        echo

	apt-get update \
	&& apt-get -y install rsyslog \
	&& cat /lando/vits/config/drupal10/rsyslog.conf >> /etc/rsyslog.conf \
	&& sed -i '/imklog/s/^/#/' /etc/rsyslog.conf && rsyslogd \
; fi

#echo
#echo -e "${YELLOW}Setting up Drupal to output to syslog ...${NORMAL}"
#echo

#sed -i "s/config_dev_syslog']\['status'] = FALSE/config_dev_syslog']\['status'] = TRUE/" /app/docroot/sites/default/settings.local.php
#${DRUSH} en syslog
#${DRUSH} cset syslog.settings facility 184
#${DRUSH} cr


## Andrew - commented out the below - the service needs to start in the run_as_root rather than build_as_root command.

#echo
#echo -e "${YELLOW}Starting rsyslog service ...${NORMAL}"
#echo
#
#service rsyslog start;
#
#echo
#echo -e "${YELLOW}Checking rsyslog service status ...${NORMAL}"
#RSYSLOG_STATUS=`service rsyslog status`
#echo -e "${CYAN} ${RSYSLOG_STATUS} ${NORMAL}"
#echo
