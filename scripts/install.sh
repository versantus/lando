#!/bin/bash

# Script to install a clean Drupal site in lando.
#
# Usage:
#   Create a new site folder and cd into it
#   Create a new .lando.yml
#     eg.   curl -o .lando.yml https://bitbucket.org/versantus/lando/raw/master/landofiles/.lando.yml
#   Save this install script into the same project root dir, naming it 'install.sh'
#     eg.   curl -o install.sh https://bitbucket.org/versantus/lando/raw/master/scripts/install.sh
#   (optionally) In your copied install.sh:
#     Change the PROJECT_VERSION to the version of Drupal that you want to install
#     Change the SITE_INSTALL_PROFILE to the site profile that you want to install
#   Run vits_setup
#   Run lando rebuild -y

# Project name examples are  drupal/recommended-project or apigee/devportal-kickstart-project
PROJECT_NAME="drupal/recommended-project"
PROJECT_VERSION="^10.0"
#  Install profile can be blank for default.  Example profiles include "minimal" and "demo_umami"
SITE_INSTALL_PROFILE=""


WHITE="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"


echo-red () { echo -e "${RED}$1${WHITE}"; }
echo-green () { echo -e "${GREEN}$1${WHITE}"; }
echo-yellow () { echo -e "${YELLOW}$1${WHITE}"; }
echo-purple () { echo -e "${PURPLE}$1${WHITE}"; }

echo-purple "Running install.sh.  Project is ${PROJECT_NAME}. Project version is ${DRUPAL_VERSION}. Install profile is \"${SITE_INSTALL_PROFILE}\" "
echo-yellow "Please note: the script assumes that you have set the webroot to 'docroot' in your .lando.yml file "
sleep 1

echo-purple "Running composer create-project ${PROJECT_NAME}:${PROJECT_VERSION} "
rm -fr /tmp/createproject 2>/dev/null

# Create the project in a temp dir because the create-project command won't work in a non-empty directory.
composer create-project --no-interaction ${PROJECT_NAME}:${PROJECT_VERSION} /tmp/createproject

# Now move everything from temp dir to our app dir.
mv /tmp/createproject/* /app/
mv /tmp/createproject/.gitattributes /app/
mv /tmp/createproject/.editorconfig /app/

# Fix the webroot in composer.json - recommended-project assumes 'web' instead of 'docroot'
sed -i 's/web\//docroot\//g' /app/composer.json
mv /app/web /app/docroot

echo-purple "Running composer require drush/drush "
composer --working-dir="/app" --no-interaction require drush/drush

cd /app

# Set up settings.php 
echo-purple "Setting up settings.php "
curl -o /app/docroot/sites/default/settings.php https://bitbucket.org/versantus/acquia_config/raw/live/settings_files/settings.php
curl -o /app/docroot/sites/default/settings.acquia.php https://bitbucket.org/versantus/acquia_config/raw/live/settings_files/settings.acquia.php
curl -o /app/docroot/sites/default/settings.lando.php https://bitbucket.org/versantus/acquia_config/raw/live/settings_files/settings.lando.php
cp /lando/vits/config/drupal9/development.services.yml /app/docroot/sites/default/development.services.yml

echo-purple "Creating .gitignore "
curl -o /app/.gitignore https://bitbucket.org/versantus/acquia_config/raw/live/gitignore/dot_gitignore

echo-purple "Creating salt "
/app/vendor/bin/drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55)' > /app/salt.txt

echo-purple "Running site:install ${SITE_INSTALL_PROFILE} "
/app/vendor/bin/drush --yes site:install ${SITE_INSTALL_PROFILE}

echo ""
echo-purple "Here's a login link: "
echo -e "${YELLOW}"
/app/vendor/bin/drush --yes uli
echo -e "${WHITE}"
