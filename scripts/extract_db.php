<?php

# Extracts a database that has been copied from the NAS.

$LIVE_SERVER_HOST = getenv('LIVE_SERVER_HOST');
$LIVE_SERVER_USER = getenv('LIVE_SERVER_USER');
$PRIMARY_SITE_DOMAIN = getenv('PRIMARY_SITE_DOMAIN');
$LIVE_SITE_DB = getenv('LIVE_SITE_DB');

$LANDO_ROOT = '/app/';
if (is_dir('/app/drupal')) {
  # Drupal 9 site
  $LANDO_ROOT = '/app/drupal';
}

$backup_tar_file = $LANDO_ROOT . '/sql/' .  $PRIMARY_SITE_DOMAIN . '.tar.gz';
$save_to_dir = $LANDO_ROOT . '/sql';
$save_to_filename = $PRIMARY_SITE_DOMAIN . '.live' . '.sql';

if (! is_dir($save_to_dir)) {
  if (!mkdir($save_to_dir) && !is_dir($save_to_dir)) {
    throw new \RuntimeException(sprintf('Directory "%s" was not created', $save_to_dir));
  }
}
extract_db($backup_tar_file, $LIVE_SERVER_USER, $LIVE_SERVER_USER,
  $save_to_dir . '/' . $save_to_filename, $PRIMARY_SITE_DOMAIN);



function extract_db($backup_tar_file, $live_site_db, $live_server_user,
                    $save_to_file, $primary_site_domain) {
  $local_extraction_dir = '/tmp';

  if ($backup_tar_file) {
    log_message('  Backup tar file is: ' . $backup_tar_file, 'ok');

    log_message('  Searching for database ' . $live_site_db . '  in tar file', 'ok');
    $find_db_in_tar_cmd = 'tar -tzf ' . $backup_tar_file . ' | grep ' . $primary_site_domain . '"_mysql_' . '" | tail -n 1';
    $db_file_in_tar = exec($find_db_in_tar_cmd);
//    if (!$db_file_in_tar) {
//      $find_db_in_tar_cmd = 'tar --wildcards -tzf ' . $backup_tar_file . ' "database/*.sql" | tail -n 1';
//      $db_file_in_tar = exec($find_db_in_tar_cmd);
//    }

    if ($db_file_in_tar) {
      log_message('  Found database backup file: ' . $db_file_in_tar, 'ok');
      $extracted_db_file = $local_extraction_dir . '/' . $live_server_user . '_live.sql.gz';
      if (file_exists($extracted_db_file)) {
        // Remove old copy if exists.
        unlink($extracted_db_file);
      }
      $db_extraction_cmd = 'tar -xvzf ' . $backup_tar_file . ' --to-stdout ' . $db_file_in_tar . ' >' . $extracted_db_file;
      log_message('  Extracting db', 'ok');
      $result = exec($db_extraction_cmd);
      if (file_exists($extracted_db_file)) {
        // gunzip
        exec('gunzip -f ' . $extracted_db_file);
        $extracted_db_file = str_replace('.gz', '', $extracted_db_file);
        log_message('  Extracted db from backup: ' . $extracted_db_file, 'ok');
        exec('mv ' . $extracted_db_file . ' ' . $save_to_file);
        log_message('  Renamed backup to: ' . $save_to_file, 'ok');

        return $save_to_file;
      }
      else {
        log_message('Unable to extract backup db from tar file: ' . $extracted_db_file, 'error');
      }
    }
    else {
      log_message('Unable to locate db in tar. Used cmd: ' . $find_db_in_tar_cmd, 'error');
    }
  }
  else {
    log_message('Unable to find backup tar file: ' . $backup_tar_file, 'error');
  }
}


function log_message($message, $error_level) {
  print " " . $message . PHP_EOL;
}
