#!/usr/bin/env bash

NORMAL="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

DRUSH="/app/drupal/vendor/drush/drush/drush --root=/app/drupal/web -y "

if [ ! -d /app/drupal/sql ]; then mkdir /app/drupal/sql; fi

echo -e "${YELLOW} Copying database from backup server ... ${NORMAL}"
rsync -avlhP -e "ssh -p ${BACKUP_SERVER_PORT}" ${BACKUP_SERVER_USER}@${BACKUP_SERVER}:${BACKUP_SERVER_DIR}/sanitized/${PRIMARY_SITE_DOMAIN}.sql.gz /app/drupal/sql/${PRIMARY_SITE_DOMAIN}.sql.gz
if [ $? -eq 0 ]; then
  : # success
else
  echo -e "${RED} Sorry, something went wrong.  Couldn't copy database.  Command to download it was: ${YELLOW}rsync -avlhP -e \"ssh -p ${BACKUP_SERVER_PORT}\" ${BACKUP_SERVER_USER}@${BACKUP_SERVER}:${BACKUP_SERVER_DIR}/sanitized/${PRIMARY_SITE_DOMAIN}.sql.gz /app/drupal/sql/${PRIMARY_SITE_DOMAIN}.sql.gz ${NORMAL}";
  echo -e "${RED} Please check the PRIMARY_SITE_DOMAIN variable in your .lando.yml file.";
  echo -e "${RED} It could be this site does not have a sanitized db.  If this is the case try vits_copy_unsanitized instead. ${NORMAL}";
  exit;
fi

DB_CHECK=`zcat /app/drupal/sql/${PRIMARY_SITE_DOMAIN}.sql.gz | tail -n 2 | grep "SET FOREIGN_KEY_CHECKS = 1" | wc -l`
if [ "${DB_CHECK}" == "0" ]; then echo -e "${RED} Sorry, something went wrong.  Couldn't find 'SET FOREIGN_KEY_CHECKS = 1' in sql file.  Command was: ${YELLOW}zcat /app/drupal/sql/${PRIMARY_SITE_DOMAIN}.sql.gz | tail -n 2 | grep "UNLOCK TABLES" | wc -l ${NORMAL}"; exit; fi

echo -e "   Database copied to drupal/sql/${PRIMARY_SITE_DOMAIN}.sql.gz ${NORMAL}"

echo -e "${YELLOW} Extracting database from gz ... ${NORMAL}"
gunzip -kf /app/drupal/sql/${PRIMARY_SITE_DOMAIN}.sql.gz

echo -e "${YELLOW} Dropping existing tables ... ${NORMAL}"
${DRUSH} sql-drop

echo -e "${YELLOW} Importing database dump ... ${NORMAL}"
pv /app/drupal/sql/${PRIMARY_SITE_DOMAIN}.sql | ${DRUSH} sqlc
rm /app/drupal/sql/${PRIMARY_SITE_DOMAIN}.sql

# Fix user 0 entry in users table, if there ought to be one.  https://versantus.atlassian.net/browse/DEVOPS-89
USER0_SHOULD_EXIST=`${DRUSH}sql-query "SELECT COUNT(uid) FROM users_field_data WHERE uid=0;" | sed -e 's/[[:space:]]*//g'`
USER0_DOES_EXIST=`${DRUSH} sql-query "SELECT COUNT(uid) FROM users WHERE uid=0;" | sed -e 's/[[:space:]]*//g'`

if [ "${USER0_SHOULD_EXIST}" = "1" ]
then
    if [ "${USER0_DOES_EXIST}" = "0" ]
    then
        echo "Fixing user 0"
        RESULT=`${DRUSH} sql-query "UPDATE users SET uid=0 WHERE uid=(SELECT MAX(uid) FROM users) LIMIT 1"`
        USER0_NOW_EXISTS=`${DRUSH} sql-query "SELECT COUNT(uid) FROM users WHERE uid=0;" | sed -e 's/[[:space:]]*//g'`
        if [ "${USER0_NOW_EXISTS}" = "1" ]
        then
	          echo "Fixed users table entry for user 0"
        else
	          echo "Failed to fix user 0"
        fi
    fi
    # Fix mail/password for user 0
    RESULT=`${DRUSH} sql-query "UPDATE users_field_data SET mail=NULL, pass=NULL, name='',init=NULL WHERE uid=0;"`
fi


echo -e "${YELLOW} Updating database ... ${NORMAL}"
${DRUSH} updatedb

echo -e "${YELLOW} Enabling stage file proxy ... ${NORMAL}"
${DRUSH} en stage_file_proxy
${DRUSH} cset stage_file_proxy.settings origin ${LIVE_SITE_URL}

echo -e "${YELLOW} Clearing cache ... ${NORMAL}"
${DRUSH} cr

if [ -f /app/.lando.post_copy_live.sh ]; then
    echo -e "${YELLOW} Running /app/.lando.post_copy_live.sh ... ${NORMAL}"
    chmod +x /app/.lando.post_copy_live.sh
    /app/.lando.post_copy_live.sh
fi

echo -e "${YELLOW} Done! Here's an admin login link: ${GREEN} ` ${DRUSH} uli`"
#echo -e "${YELLOW} If you are not the develop branch or have locally modified config you may want to run ${GREEN}lando drush cim${YELLOW} now. ${NORMAL}"
 

