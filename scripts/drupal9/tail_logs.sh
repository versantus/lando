#!/usr/bin/env bash

NORMAL="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

DRUSH="/app/drupal/vendor/drush/drush/drush --root=/app/drupal/web -y "

# This file is configured in .lando.base.yml. Run: lando logs-drupal.
# It will enable the syslog module using config_split module,
# start the rsyslog service and tail the log file. This is really
# only necessary because Drush 10 removed the ability to tail `drush wd-show`.

echo
echo -e "${YELLOW}Enabling syslog module ...${NORMAL}"
echo

mkdir /app/drupal/logs 2>/dev/null
touch /app/drupal/logs/drupal.log

ALREADY_SETUP=`grep -F 'local7.* /app/drupal/logs/drupal.log' /etc/rsyslog.conf`

echo
echo -e "${YELLOW}Checking if syslog is already installed ... ${NORMAL}"
echo


if [ "${ALREADY_SETUP}" == "" ]; then
        echo
        echo -e "${YELLOW}Installing syslog module ...${NORMAL}"
        echo

	apt-get update \
	&& apt-get -y install rsyslog \
	&& cat /lando/vits/config/drupal9/rsyslog.conf >> /etc/rsyslog.conf \
	&& sed -i '/imklog/s/^/#/' /etc/rsyslog.conf && service rsyslog start \
; fi

echo
echo -e "${YELLOW}Setting up Drupal to output to syslog ...${NORMAL}"
echo

#sed -i "s/config_dev_syslog']\['status'] = FALSE/config_dev_syslog']\['status'] = TRUE/" /app/web/sites/default/settings.local.php
${DRUSH} en syslog
${DRUSH} cset syslog.settings facility 184
${DRUSH} cset syslog.settings format '!type|!request_uri|!message'
${DRUSH} cr

echo
echo -e "${YELLOW}Starting rsyslog service ...${NORMAL}"
echo

service rsyslog start;

echo
echo -e "${YELLOW}Tailing logs...${NORMAL}"
echo
tail -f /app/drupal/logs/drupal.log | tr -s '|' \\t
