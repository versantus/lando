#!/bin/bash

export CURRENT_LANDO_VER=`lando version 2>/dev/null | sed 's#v##'`
export LANDO_LATEST_URL=`curl -w "%{url_effective}\n" -I -L -s -S https://github.com/lando/lando/releases/latest -o /dev/null`
export LANDO_LATEST_VER=`echo ${LANDO_LATEST_URL} | sed 's#.*/v##'`

if [ "${CURRENT_LANDO_VER}" == "${LANDO_LATEST_VER}" ]; then
    echo "You're already on the latest version: ${CURRENT_LANDO_VER}"
else
    echo "Upgrading from ${CURRENT_LANDO_VER} to ${LANDO_LATEST_VER}"
    echo "You'll need to enter your password for me to run a sudo command"
    curl -L -o /tmp/lando-latest.deb "https://github.com/lando/lando/releases/download/v${LANDO_LATEST_VER}/lando-x64-v${LANDO_LATEST_VER}.deb"
    sudo dpkg -i /tmp/lando-latest.deb
    export CURRENT_LANDO_VER=`lando version 2>/dev/null | sed 's#v##'`
    echo "Installed lando version: ${CURRENT_LANDO_VER}"
fi;

