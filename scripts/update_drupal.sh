#! /bin/bash

# Summary:
# This script updates a Drupal project's composer.json.
# It removes drupal/core if required, set drupal/core-* to a specified version,
# updates drupal/core-* drush/drush and all dependencies.
# It then check for any known vulnerabilities and update the packages.

# Usage:
# ./update_drupal.sh <composer_path> <drupal_version>

# Check if the path provided as an argument contains composer.json
if [ ! -z "$1" ] && [ ! -f "$1/composer.json" ]; then
  echo "Error: The path $1 does not contain the composer.json file."
  exit 1
fi

# Check if a parameter was submitted with the command
if [ -z "$2" ]; then
  echo "Error: please provide the drupal version to update to."
  exit 1
fi

# Check if the drupal version is correct
if grep -qE "^[\^~][0-9]+(\.[0-9]+)?(\.[0-9]+)?$" <<< "$2"; then
  DRUPAL_VERSION="$2"
else
  echo "Error: The drupal version needs to be in the format (^|~)\d+(\.\d+)?(\.\d+)?."
  exit 1
fi

COMPOSER_PATH="$1"
COMPOSER="composer --working-dir=$COMPOSER_PATH "

# Check if composer.json contains a require entry for drupal/core
echo -n "Checking drupal/core is required in composer.json: "
if [ -n "$($COMPOSER show -D drupal/core 2>/dev/null)" ]; then
  echo "Found -> Running composer remove drupal/core..."
  $COMPOSER remove --no-update drupal/core
else
  echo "Not found √"
fi


# Set drupal version to the provided one
NO_DEV_PACKAGES="$($COMPOSER show --direct --name-only --no-dev | grep '^drupal/core-')"
# List all packages matching drupal/core-* and update their version requirement
for package in $NO_DEV_PACKAGES; do
  $COMPOSER require --no-update "$package:$DRUPAL_VERSION"
done

ALL_PACKAGES="$($COMPOSER show --direct --name-only | grep '^drupal/core-')"
for package in $ALL_PACKAGES; do
  if ! echo "$NO_DEV_PACKAGES" | grep -q "^$package$"; then
    $COMPOSER require --no-update --dev "$package:$DRUPAL_VERSION"
  fi
done

$COMPOSER update -W "drupal/core-*" drush/drush

# Find any packages that need to be updated due to CVE vulnerabilities
PACKAGES_TO_UPDATE="$($COMPOSER audit | grep Package | awk '{print $4}' | sort | uniq)"

if [ -n "$PACKAGES_TO_UPDATE" ]; then
  $COMPOSER update -W "$PACKAGES_TO_UPDATE"
fi
