#!/usr/bin/env bash

# Abort if anything fails
#set -e

# Console colors
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[34m"
CYAN="\033[36m"
PURPLE="\033[35m"
WHITE="\033[0m"

echo-red () { echo -e "${RED}$1${WHITE}"; }
echo-green () { echo -e "${GREEN}$1${WHITE}"; }
echo-yellow () { echo -e "${YELLOW}$1${WHITE}"; }
echo-blue () { echo -e "${BLUE}$1${WHITE}"; }
echo-cyan () { echo -e "${CYAN}$1${WHITE}"; }
echo-purple () { echo -e "${PURPLE}$1${WHITE}"; }

SITE_IS_ON_ACQUIA=`ls /app/.acquia-cli.yml 2>/dev/null`
RECIPE=$(grep \^recipe\: .lando.yml | tail -n 1 | sed  -e 's/recipe://' | sed -e 's/ *//g')

if [ "${ACQUIA_APPLICATION}" == "" ]; then
  if [ "${SITE_IS_ON_ACQUIA}" == "" ]; then
  	# Use the traditional vits_copy_live method of getting a linode backup
    echo-yellow "Not on Acquia so falling back to traditional copy_sanitized.sh method for ${RECIPE}"
	  /lando/vits/scripts/${RECIPE}/copy_sanitized.sh
  else
	  echo-red "ACQUIA_APPLICATION env var is not defined"
    echo-yellow "Add ACQUIA_APPLICATION to the services: -> appserver: -> overrides: -> environment: section of .lando.yml and run lando rebuild -y"
  fi
  exit 1;
fi

# Tidy tmp dir
rm /tmp/*.sql.gz 2>/dev/null
rm /tmp/*.sql 2>/dev/null


# Read command line args
ENVIRONMENT='prod'
SITE=''
ONDEMAND=''
VERBOSE='false'

print_usage() {
  echo "Usage:"
  echo "  [no arguments required to copy database from live - just set ACQUIA_APPLICATION in the site's .lando.yml] "
  echo ""
  echo "  Optional args:"
  echo "  -e [environment]"
  echo "     Copy database from a non-live environment.  environment can be either 'dev' or 'test'  ('prod' is assumed by default)"
  echo "  -s [site]"
  echo "     For multisites only.  'site' is the name of a site's database to copy"
  echo "  -h"
  echo "     Display this help page "
  echo "  -o"
  echo "     Request an on-demand copy of the database. By default the system will use the last daily backup of the database."
  echo "  -v"
  echo "     Verbose output"
}

while getopts 'e:s:ov' flag; do
  case "${flag}" in
    e) ENVIRONMENT="${OPTARG}" ;;
    s) SITE="${OPTARG}" ;;
    o) ONDEMAND='--on-demand' ;;
    v) VERBOSE='true' ;;
    h) print_usage; exit 1 ;;
    *) print_usage
       exit 1 ;;
  esac
done

if [ "${ENVIRONMENT}" == "staging" ]; then
  ENVIRONMENT="test"
fi
if [ "${ENVIRONMENT}" == "stage" ]; then
  ENVIRONMENT="test"
fi

echo-yellow "Using the following settings:"
echo-yellow "  application: ${GREEN} ${ACQUIA_APPLICATION}"
echo-yellow "  environment: ${GREEN} ${ENVIRONMENT}"
if [ "${SITE}" != "" ]; then
  echo-yellow "  site:        ${GREEN} ${SITE}"
fi
if [ "${ONDEMAND}" != "" ]; then
  echo-yellow "  on-demand:   ${GREEN} will make an on-demand copy of the database"
fi

echo-yellow "Downloading database from Acquia"
if [ "${VERBOSE}" == "true" ]; then
  echo-purple "  acli pull:database ${ACQUIA_APPLICATION}.${ENVIRONMENT} ${SITE} --no-import ${ONDEMAND}"
fi
acli pull:database ${ACQUIA_APPLICATION}.${ENVIRONMENT} ${SITE} --no-import ${ONDEMAND}

DB_GZ=`ls /tmp/${ENVIRONMENT}-*.sql.gz 2>/dev/null`
if [ "${DB_GZ}" != "" ]; then
    # Gunzip
    echo-yellow "Unzipping database"
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  gunzip -f ${DB_GZ}"
    fi
    gunzip -f ${DB_GZ}
    DB=`ls /tmp/${ENVIRONMENT}-*.sql`

    # Remove any DEFINER lines
    echo-yellow "Cleaning database"
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  cat ${DB} | grep -v DEFINER >${DB}.clean"
      echo-purple "  mv ${DB}.clean ${DB}"
    fi
    cat ${DB} | grep -v DEFINER >${DB}.clean
    mv ${DB}.clean ${DB}

    DRUSH="/app/vendor/bin/drush --root=/app/docroot -y "
    if [ "${SITE}" != "" ]; then
      #DRUSH="/app/vendor/bin/drush --root=/app/docroot/sites/${SITE} -y "
      DRUSH="/app/vendor/bin/drush --root=/app/docroot/sites/${SITE} @${SITE}.local -y "
    fi

    # Inject database into drupal
    echo-yellow "Dropping existing database"
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  ${DRUSH} sql-drop"
    fi
    ${DRUSH} sql-drop

    echo-yellow "Importing database into lando"
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  pv ${DB} | ${DRUSH} sqlc"
    fi
    pv ${DB} | ${DRUSH} sqlc

    # Sanitize database
    echo-yellow "Sanitizing database"
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  ${DRUSH} sql-sanitize"
    fi
    ${DRUSH} sql-sanitize

    echo-yellow "Clearing cache"
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  ${DRUSH} cr"
    fi
    ${DRUSH} cr

    echo-yellow " Updating database ... "
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  ${DRUSH} updatedb"
    fi
    ${DRUSH} updatedb

    if [ "${LIVE_SITE_URL}" != "" ]; then
      if [ "${SITE}" == "" ]; then
        echo-yellow " Enabling stage file proxy ... "
        if [ "${VERBOSE}" == "true" ]; then
          echo-purple "  ${DRUSH} en stage_file_proxy"
          echo-purple "  ${DRUSH} cset stage_file_proxy.settings origin ${LIVE_SITE_URL}"
        fi
        ${DRUSH} en stage_file_proxy
        ${DRUSH} cset stage_file_proxy.settings origin ${LIVE_SITE_URL}
      else
        echo-yellow "Skipping stage file proxy - not sure what the live site url is for multisite sites"
      fi
    else
      echo-yellow "Skipping stage file proxy - no LIVE_SITE_URL env var is defined in the .lando.yml"
    fi

    echo-yellow "Clearing cache"
    if [ "${VERBOSE}" == "true" ]; then
      echo-purple "  ${DRUSH} cr"
    fi
    ${DRUSH} cr

    if [ -f /app/.lando.post_copy_live.sh ]; then
      echo-yellow " Running /app/.lando.post_copy_live.sh ..."
      chmod +x /app/.lando.post_copy_live.sh
      /app/.lando.post_copy_live.sh
    fi

    # ULI
    echo-yellow "Here's a one-link login link:"
    ${DRUSH} uli

    # Tidy up
    rm ${DB}
    echo-green "All done"
else
    echo-red "No gzipped database found"
fi
