#!/usr/bin/env bash

NORMAL="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

DRUSH="drush --root=/app/public_html -y "

if [ ! -d /app/sql ]; then mkdir /app/sql; fi

echo -e "${YELLOW} Copying database from backup server ... ${NORMAL}"
rsync -alP -e "ssh -p ${BACKUP_SERVER_PORT}" ${BACKUP_SERVER_USER}@${BACKUP_SERVER}:${BACKUP_SERVER_DIR}/sanitized/${PRIMARY_SITE_DOMAIN}.sql.gz /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz 1>/dev/null

if [ ! -f /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz ]; then echo "Sorry, something went wrong.  Command was: rsync -alP -e \"ssh -p ${BACKUP_SERVER_PORT}\" ${BACKUP_SERVER_USER}@${BACKUP_SERVER}:${BACKUP_SERVER_DIR}/sanitized/${PRIMARY_SITE_DOMAIN}.sql.gz /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz" ; echo "Please check the PRIMARY_SITE_DOMAIN variable in your .lando.yml file." ; echo "It could be this site does not have a sanitized db.  If this is the case try vits_copy_unsanitized instead." ; exit; fi

echo -e "   Database copied to drupal/sql/${PRIMARY_SITE_DOMAIN}.sql.gz ${NORMAL}"

echo -e "${YELLOW} Extracting database from gz ... ${NORMAL}"
gunzip -f /app/sql/${PRIMARY_SITE_DOMAIN}.sql.gz

echo -e "${YELLOW} Dropping existing tables ... ${NORMAL}"
${DRUSH} sql-drop

echo -e "${YELLOW} Importing database dump ... ${NORMAL}"
pv /app/sql/${PRIMARY_SITE_DOMAIN}.sql | ${DRUSH} sqlc

echo -e "${YELLOW} Updating database ... ${NORMAL}"
${DRUSH} updatedb

echo -e "${YELLOW} Enabling stage file proxy ... ${NORMAL}"
${DRUSH} en stage_file_proxy
${DRUSH} vset stage_file_proxy.settings origin ${LIVE_SITE_URL}

echo -e "${YELLOW} Clearing cache ... ${NORMAL}"
${DRUSH} cc all

if [ -f /app/.lando.post_copy_live.sh ]; then
    echo -e "${YELLOW} Running /app/.lando.post_copy_live.sh ... ${NORMAL}"
    /app/.lando.post_copy_live.sh
fi

echo -e "${YELLOW} Done! Here's an admin login link: ${GREEN} ` ${DRUSH} uli`"


