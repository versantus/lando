#!/usr/bin/env bash

NORMAL="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

DRUSH="drush --root=/app/public_html -y "

if [ ! -d /app/sql ]; then mkdir /app/sql; fi

echo -e "${YELLOW} Copying database from backup server ... ${NORMAL}"
rsync -alP -e "ssh -p ${BACKUP_SERVER_PORT}" ${BACKUP_SERVER_USER}@${BACKUP_SERVER}:${BACKUP_SERVER_DIR}/${LIVE_SERVER_HOST}/database/${PRIMARY_SITE_DOMAIN}.tar.gz /app/sql/${PRIMARY_SITE_DOMAIN}.tar.gz 1>/dev/null

if [ ! -f /app/sql/${PRIMARY_SITE_DOMAIN}.tar.gz ]; then echo "Sorry, something went wrong" ; exit; fi
echo -e "   Database copied to drupal/sql/${PRIMARY_SITE_DOMAIN}.tar.gz ${NORMAL}"

echo -e "${YELLOW} Extracting database from tar ... ${NORMAL}"
php /lando/vits/scripts/extract_db.php ${LIVE_SERVER_HOST} ${PRIMARY_SITE_DOMAIN}
if [ ! -f /app/sql/${PRIMARY_SITE_DOMAIN}.live.sql ]; then echo "Sorry, something went wrong" ; exit; fi

echo -e "${YELLOW} Dropping existing tables ... ${NORMAL}"
${DRUSH} sql-drop

echo -e "${YELLOW} Importing database dump ... ${NORMAL}"
pv /app/sql/${PRIMARY_SITE_DOMAIN}.live.sql | ${DRUSH} sqlc

echo -e "${YELLOW} Updating database ... ${NORMAL}"
${DRUSH} updatedb

echo -e "${YELLOW} Enabling stage file proxy ... ${NORMAL}"
${DRUSH} en stage_file_proxy
${DRUSH} vset stage_file_proxy.settings origin ${LIVE_SITE_URL}


echo -e "${YELLOW} Clearing cache ... ${NORMAL}"
${DRUSH} cc all

if [ -f /app/.lando.post_copy_live.sh ]; then
    echo -e "${YELLOW} Running /app/.lando.post_copy_live.sh ... ${NORMAL}"
    /app/.lando.post_copy_live.sh
fi

echo -e "${YELLOW} Done! Here's an admin login link: ${GREEN} ` ${DRUSH} uli`"


