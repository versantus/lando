#!/bin/bash

# If any files in /app/logs dir are bigger than [a big thing] then delete them.
# If any files in /app/logs dir are older than [an old thing] then delete them.

LOG_DIR="/app/logs"
MAX_ALLOWED_FILESIZE="100M"
MAX_ALLOWED_AGE_IN_DAYS="60"


WHITE="\033[0m"
BLUE="\033[34m"
CYAN="\033[36m"
GREEN="\033[32m"
PURPLE="\033[35m"
RED="\033[31m"
YELLOW="\033[33m"

echo-red () { echo -e "${RED}$1${WHITE}"; }
echo-green () { echo -e "${GREEN}$1${WHITE}"; }
echo-yellow () { echo -e "${YELLOW}$1${WHITE}"; }

echo-yellow "Clearing out any log files in ${LOG_DIR} that are bigger than ${MAX_ALLOWED_FILESIZE} ... "

# Using script gleamed from https://unix.stackexchange.com/questions/381391/find-biggest-files-and-delete-automatically
find ${LOG_DIR} -type f -size +${MAX_ALLOWED_FILESIZE} -printf '%s\t%p; \0' | cut  -z -f 2- | xargs -0 -r echo rm -f --
find ${LOG_DIR} -type f -size +${MAX_ALLOWED_FILESIZE} -printf '%s\t%p\0' | cut  -z -f 2- | xargs -0 -r rm -f --

echo-yellow "Clearing out any log files in ${LOG_DIR} that are older than ${MAX_ALLOWED_AGE_IN_DAYS} days ... "
find ${LOG_DIR} -type f -mtime +${MAX_ALLOWED_AGE_IN_DAYS} -printf '%s\t%p; \0' | cut  -z -f 2- | xargs -0 -r echo rm -f --
find ${LOG_DIR} -type f -mtime +${MAX_ALLOWED_AGE_IN_DAYS} -printf '%s\t%p\0' | cut  -z -f 2- | xargs -0 -r rm -f --

